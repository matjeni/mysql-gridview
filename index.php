
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
<link rel="stylesheet" href="css/jqx.base.css" type="text/css" />
<link rel="stylesheet" href="css/jqx.classic.css" type="text/css" />
<script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
<script type="text/javascript" src="js/jqxcore.js"></script>
<script type="text/javascript" src="js/jqxbuttons.js"></script>
<script type="text/javascript" src="js/jqxscrollbar.js"></script>
<script type="text/javascript" src="js/jqxmenu.js"></script>
<script type="text/javascript" src="js/jqxdata.js"></script>
<script type="text/javascript" src="js/jqxgrid.js"></script>
<script type="text/javascript" src="js/jqxgrid.selection.js"></script>
    </head>
<!-- 
sowertech – TO ENCOURAGE GROWTH – © 2013
author: |O|O
description: this is a gridview that populates from northwind database I have used Customers table to fill the view. the tutorial was found at http://www.jqwidgets.com/
date: 8 Jan 2013
-->
        <body>
<h2>MySql Gridview	</h2>
 <div id="jqxgrid"></div>
<script type="text/javascript">
     $(document).ready(function () {
         // prepare the data
         var source =
         {
             datatype: "json",
             datafields: [
                 { name: 'CompanyName'},
                 { name: 'ContactName'},
                 { name: 'ContactTitle'},
                 { name: 'Address'},
                 { name: 'City'},
             ],
             url: 'data.php'
         };
 
         $("#jqxgrid").jqxGrid(
         {
             source: source,
             theme: 'classic',
             columns: [
               { text: 'Company Name', datafield: 'CompanyName', width: 250},
               { text: 'ContactName', datafield: 'ContactName', width: 150 },
               { text: 'Contact Title', datafield: 'ContactTitle', width: 180 },
               { text: 'Address', datafield: 'Address', width: 200 },
               { text: 'City', datafield: 'City', width: 120 }
           ]
         });        
     });
 </script>
        </body>
</html>

